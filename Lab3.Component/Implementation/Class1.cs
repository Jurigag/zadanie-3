﻿using Lab3.Contract;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Implementation
{
    class Dzwiek:IFonia
    {
        public void getSound()
        {
            Debug.WriteLine("Pobieram dźwięk z układu dźwiękowego");
        }

        public void volumeUp(double s)
        {
            Debug.WriteLine("Podgłośniono o {0}", s);
        }

        public void volumeDown(double s)
        {
            Debug.WriteLine("Przyciszono o {0}", s);
        }
    }
}
