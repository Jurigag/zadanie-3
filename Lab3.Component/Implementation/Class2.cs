﻿using Lab3.Contract;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Implementation
{
    class Wizja:IWizja
    {
        public void getVision()
        {
            Debug.WriteLine("Pobieram obraz z układu obrazu");
        }

        public void showVision()
        {
            Debug.WriteLine("Wyświetlam obraz na kineskopie");
        }

        public void clearVision()
        {
            Debug.WriteLine("Czyszczę obraz z zakłóceń");
        }
    }
}
