﻿using Lab3.Contract;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Implementation
{
    class Program:IProgramator
    {
        public void changeChannel()
        {
            Debug.WriteLine("Zmieniam kanał");
        }

        public void addChannel()
        {
            Debug.WriteLine("Dodaję kanał");
        }

        public void removeChannel()
        {
            Debug.WriteLine("Ususwam kanał");
        }
    }
}
