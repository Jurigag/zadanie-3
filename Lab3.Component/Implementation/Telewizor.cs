﻿using Lab3.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Implementation
{
    public class Telewizor
    {
        private double volume;

        public double Volume
        {
            get { return volume; }
            set { volume = value; }
        }
        
        private Dzwiek dzwiek = new Dzwiek();
        private Wizja wizja = new Wizja();
        private Program program = new Program();

        public static IFonia GetDzwiek(object component)
        {
            return (component as Telewizor).dzwiek;
        }

        public static IWizja GetWizja(object component)
        {
            return (component as Telewizor).wizja;
        }

        public static IProgramator GetProgram(object component)
        {
            return (component as Telewizor).program;
        }
    }
}
