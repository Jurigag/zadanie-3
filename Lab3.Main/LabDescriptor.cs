﻿using Lab3.Contract;
using Lab3.Implementation;
using System;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        
        #endregion

        #region P1

        public static Type I1 = typeof(IWizja);
        public static Type I2 = typeof(IFonia);
        public static Type I3 = typeof(IProgramator);

        public static Type Component = typeof(Telewizor);

        public static GetInstance GetInstanceOfI1 = (component) => Telewizor.GetWizja(component);
        public static GetInstance GetInstanceOfI2 = (component) => Telewizor.GetDzwiek(component);
        public static GetInstance GetInstanceOfI3 = (component) => Telewizor.GetProgram(component);
        
        #endregion

        #region P2

        public static Type Mixin = typeof(Wzmacniacz);
        public static Type MixinFor = typeof(Telewizor);

        #endregion
    }
}
