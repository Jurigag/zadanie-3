﻿using Lab3.Contract;
using Lab3.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    public static class Wzmacniacz
    {
        public static void Wzmacniaj(this Telewizor t)
        {
            Telewizor.GetDzwiek(t).volumeUp(t.Volume);
        }
    }
}
